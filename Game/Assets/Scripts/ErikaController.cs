﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ErikaController : MonoBehaviour
{
    //Components
    private Animator _animator;
    private Rigidbody2D _rb2D;

    //Variables
    public LayerMask layerCollisionGround;
    public LayerMask layerCollisionPushable;
    public float speed = 6.49f;
    public float jumpForce = 9.54f;
    public float distanceGround = 1.39f;
    public float distanceFront = 0.59f;
    private int level;
    private Vector3 displacement;

    // Start is called before the first frame update
    void Start()
    {
        _animator = this.gameObject.GetComponent<Animator>();
        _rb2D = this.gameObject.GetComponent<Rigidbody2D>();
        if (isCheckpointSaved())
        {
            transform.position = GameManager._instance.lastCheckPointPos;
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (isOnGround())
            {
                //_animator.SetBool("OnGround", false);
                jump();
            }
        }
    }
    // Update is called once per frame
    void FixedUpdate()
     {
        
         if (GameManager._instance.getGameState() == GameStates.playing) {
             //Movement
             displacement = new Vector3(Input.GetAxis("Horizontal"), 0);
             transform.position += displacement * speed * Time.deltaTime;
             

             //Animator
             _animator.SetFloat("Horizontal", displacement.x);
            //_animator.SetFloat("Vertical", displacement.y);

            //Go to Checkpoint
             if (Input.GetButtonDown("Checkpoint")) {
                 if (isCheckpointSaved()) {
                     SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                     //LevelSelector.LoadScene(level);
                 }
             }

            //Load game
            if (Input.GetKeyDown(KeyCode.L)) {
                 GameManager._instance.LoadGame();
             }


            if (isOnGround()) {
                _animator.SetBool("OnGround", true);
            } else {
                _animator.SetBool("OnGround", false);
            }
         }   
     }

    bool isCheckpointSaved()
    {
        return GameManager._instance.lastCheckPointPos != new Vector3(0, 0, 0);
    }

    private void jump()
    {
        _rb2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    }

    bool isOnGround()
    {
        return Physics2D.Raycast(_rb2D.position, Vector2.down, distanceGround, layerCollisionGround);
    }

    //private void pull()
    //{
    //  Physics2D.queriesStartInColliders = false;
    //RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, distanceFront, layerCollisionPushable);

    //if (hit.collider != null && hit.collider.gameObject.tag == "Pushable" && Input.GetKeyDown(KeyCode.E)) {
    //   box = hit.collider.gameObject;
    // box.GetComponent<FixedJoint2D>().connectedBody = this.GetComponent<Rigidbody2D>();
    //box.GetComponent<FixedJoint2D>().enabled = true;

    //} else {
    //  box.GetComponent<FixedJoint2D>().enabled = false;
    //}
    //}


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Killable")) {
            Debug.Log("Dead");
            Destroy(gameObject);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if(collision.gameObject.CompareTag("Bunker")) {
            GameManager._instance.lastCheckPointPos = new Vector3(0, 0, 0);
            level = GameManager._instance.lastLevelScene;
            GameManager._instance.SaveGame();
            if (level != 3) {
                LevelSelector.LoadScene(level + 1);
            }
            else
            {
                LevelSelector.LoadScene(0);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Killable")) {
            Debug.Log("Dead");
            Destroy(gameObject);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
