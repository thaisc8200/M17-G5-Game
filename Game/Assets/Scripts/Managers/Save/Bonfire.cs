﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bonfire : MonoBehaviour
{
    public Sprite bonfireOff;
    public Sprite bonfireOn;
    private SpriteRenderer _spriteRenderer;
    private Vector3 bonfirePos;

    private void Start()
    {
        _spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        _spriteRenderer.sprite = bonfireOff;
        bonfirePos = this.gameObject.transform.position;
        bonfirePos.y++;
    }

    private void Update()
    {
        if (GameManager._instance.lastCheckPointPos != bonfirePos) {
            _spriteRenderer.sprite = bonfireOff;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) {
            GameManager._instance.lastCheckPointPos = bonfirePos;
            GameManager._instance.lastLevelScene = SceneManager.GetActiveScene().buildIndex;
            _spriteRenderer.sprite = bonfireOn;
            GameManager._instance.SaveGame();
        }
    }
}
