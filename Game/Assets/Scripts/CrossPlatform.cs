﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossPlatform : MonoBehaviour
{
    private PlatformEffector2D _pe2D;

    // Start is called before the first frame update
    void Start()
    {
        _pe2D = this.gameObject.GetComponent<PlatformEffector2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonUp("Cross")) {
            _pe2D.rotationalOffset = 0;
        }

        if (Input.GetButton("Cross")) {
            _pe2D.rotationalOffset = 180f;
        }

        if (Input.GetButtonDown("Jump")) {
            _pe2D.rotationalOffset = 0;
        } 

       
       

        
    }
}
